use chrono::prelude::*;
use entity::command::*;
use poem::error::{BadRequest, InternalServerError};
use poem::http::StatusCode;
use poem::web::{Data, Form, Html, Path, Query};
use poem::{handler, Error, IntoResponse, Result};
use sea_orm::{entity::*, query::*};
use serde::Deserialize;

use crate::AppState;

const DEFAULT_COMMANDS_PER_PAGE: usize = 10;

#[derive(Deserialize)]
pub struct CommandQuery {
    pub page: Option<usize>,
    pub per_page: Option<usize>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct CommandForm {
    pub name: String,
    pub script: String,
}

#[handler]
pub async fn create_command(
    state: Data<&AppState>,
    form: Form<CommandForm>,
) -> Result<impl IntoResponse> {
    ActiveModel {
        name: Set(form.name.to_owned()),
        script: Set(form.script.to_owned()),
        created_at: Set(Local::now().with_timezone(&FixedOffset::east(8 * 3600))),
        ..Default::default()
    }
    .save(&state.conn)
    .await
    .map_err(InternalServerError)?;

    Ok(StatusCode::FOUND.with_header("location", "/command"))
}

#[handler]
pub async fn list_command(
    state: Data<&AppState>,
    params: Query<CommandQuery>,
) -> Result<impl IntoResponse> {
    let page = params.page.unwrap_or(1);
    let per_page = params.per_page.unwrap_or(DEFAULT_COMMANDS_PER_PAGE);
    let paginator = Entity::find()
        .order_by_asc(Column::Id)
        .paginate(&state.conn, per_page);
    let num_pages = paginator.num_pages().await.map_err(BadRequest)?;
    let commands = paginator
        .fetch_page(page - 1)
        .await
        .map_err(InternalServerError)?;

    let mut ctx = tera::Context::new();
    ctx.insert("commands", &commands);
    ctx.insert("page", &page);
    ctx.insert("per_page", &per_page);
    ctx.insert("num_pages", &num_pages);

    let body = state
        .templates
        .render("command/index.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}

#[handler]
pub async fn new_command(state: Data<&AppState>) -> Result<impl IntoResponse> {
    let ctx = tera::Context::new();
    let body = state
        .templates
        .render("command/new.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}

#[handler]
pub async fn edit_command(
    state: Data<&AppState>,
    Path(id): Path<i32>,
) -> Result<impl IntoResponse> {
    let command: Model = Entity::find_by_id(id)
        .one(&state.conn)
        .await
        .map_err(InternalServerError)?
        .ok_or_else(|| Error::from_status(StatusCode::NOT_FOUND))?;

    let mut ctx = tera::Context::new();
    ctx.insert("command", &command);

    let body = state
        .templates
        .render("command/edit.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}

#[handler]
pub async fn update_command(
    state: Data<&AppState>,
    Path(id): Path<i32>,
    form: Form<CommandForm>,
) -> Result<impl IntoResponse> {
    ActiveModel {
        id: Set(id),
        name: Set(form.name.to_owned()),
        script: Set(form.script.to_owned()),
        updated_at: Set(Some(
            Local::now().with_timezone(&FixedOffset::east(8 * 3600)),
        )),
        ..Default::default()
    }
    .save(&state.conn)
    .await
    .map_err(InternalServerError)?;

    Ok(StatusCode::FOUND.with_header("location", "/command"))
}

#[handler]
pub async fn delete_command(
    state: Data<&AppState>,
    Path(id): Path<i32>,
) -> Result<impl IntoResponse> {
    let command: ActiveModel = Entity::find_by_id(id)
        .one(&state.conn)
        .await
        .map_err(InternalServerError)?
        .ok_or_else(|| Error::from_status(StatusCode::NOT_FOUND))?
        .into();
    command
        .delete(&state.conn)
        .await
        .map_err(InternalServerError)?;

    Ok(StatusCode::FOUND.with_header("location", "/command"))
}

#[handler]
pub async fn index(state: Data<&AppState>) -> Result<impl IntoResponse> {
    let ctx = tera::Context::new();
    let body = state
        .templates
        .render("index.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}
