use chrono::prelude::*;
use entity::node::*;
use poem::error::{BadRequest, InternalServerError};
use poem::http::StatusCode;
use poem::web::{Data, Html, Json, Query};
use poem::{handler, IntoResponse, Result};
use sea_orm::{entity::*, query::*};
use serde::Deserialize;

use crate::AppState;

const DEFAULT_NODES_PER_PAGE: usize = 10;

pub type Nodes = Vec<Node>;

#[derive(Clone, Debug, Deserialize)]
pub struct Node {
    pub cluster_id: String,
    pub host: String,
    pub port: i16,
    pub node_type: String,
}

#[handler]
pub async fn import_node(state: Data<&AppState>, nodes: Json<Nodes>) -> Result<impl IntoResponse> {
    if nodes.0.len() == 0 {
        return Ok(StatusCode::BAD_REQUEST);
    }
    let mut models = Vec::new();
    for node in nodes.0 {
        let m = ActiveModel {
            id: NotSet,
            cluster_id: Set(node.cluster_id),
            host: Set(node.host.clone()),
            port: Set(node.port.clone()),
            address: Set(format!("{}:{}", node.host, node.port)),
            node_type: Set(node.node_type),
            created_at: Set(Local::now().with_timezone(&FixedOffset::east(8 * 3600))),
            ..Default::default()
        };
        models.push(m);
    }

    let _ = Entity::insert_many(models)
        .exec(&state.conn)
        .await
        .map_err(InternalServerError)?;

    Ok(StatusCode::OK)
}

#[derive(Deserialize)]
pub struct NodeQuery {
    pub page: Option<usize>,
    pub per_page: Option<usize>,
    pub cluster_id: Option<String>,
    pub host: Option<String>,
}

#[handler]
pub async fn list_node(
    state: Data<&AppState>,
    params: Query<NodeQuery>,
) -> Result<impl IntoResponse> {
    let condition = match &params.0.cluster_id {
        None => Condition::all(),
        Some(cluster_id) => Condition::all().add(Column::ClusterId.contains(&cluster_id)),
    };
    let condition = match &params.0.host {
        None => condition,
        Some(host) => Condition::all().add(Column::Host.contains(&host)),
    };

    let page = params.page.unwrap_or(1);
    let per_page = params.per_page.unwrap_or(DEFAULT_NODES_PER_PAGE);
    let paginator = Entity::find()
        .filter(condition)
        .order_by_asc(Column::Id)
        .paginate(&state.conn, per_page);
    let num_pages = paginator.num_pages().await.map_err(BadRequest)?;
    let nodes = paginator
        .fetch_page(page - 1)
        .await
        .map_err(InternalServerError)?;

    let mut ctx = tera::Context::new();
    ctx.insert("nodes", &nodes);
    ctx.insert("page", &page);
    ctx.insert("per_page", &per_page);
    ctx.insert("num_pages", &num_pages);

    let body = state
        .templates
        .render("node/index.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}
