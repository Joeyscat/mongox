use std::fs::{self, File};
use std::io::{BufWriter, Write};
use std::path::PathBuf;
use std::str::FromStr;

use crate::job::batch::Batch;
use crate::job::command::CommandsJob;
use crate::utils::ser::str_to_commands;
use crate::{AppConfig, AppState};
use chrono::prelude::*;
use entity::command::*;
use entity::job;
use entity::node;
use poem::error::InternalServerError;
use poem::http::StatusCode;
use poem::web::{Data, Form, Html, Path};
use poem::{handler, Error, IntoResponse, Result};
use sea_orm::{entity::*, query::*, Condition};
use serde::Deserialize;
use serde::Serialize;
use tracing::{debug, error};

const COMMAND_JOB_LOG_ROOT: &str = "/tmp";

#[handler]
pub async fn run_command(state: Data<&AppState>, Path(id): Path<i32>) -> Result<impl IntoResponse> {
    let command: Model = Entity::find_by_id(id)
        .one(&state.conn)
        .await
        .map_err(InternalServerError)?
        .ok_or_else(|| Error::from_status(StatusCode::NOT_FOUND))?;

    let mut ctx = tera::Context::new();
    ctx.insert("command", &command);

    let body = state
        .templates
        .render("command/run-command.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}

#[derive(Clone, Debug, Deserialize)]
pub struct RunCommandForm {
    pub command_id: i32,
    pub cluster_id: String,
    pub node_addresses: String,
}

#[handler]
pub async fn do_run_command(
    state: Data<&AppState>,
    form: Form<RunCommandForm>,
) -> Result<impl IntoResponse> {
    let command: Model = Entity::find_by_id(form.command_id)
        .one(&state.conn)
        .await
        .map_err(InternalServerError)?
        .ok_or_else(|| Error::from_status(StatusCode::NOT_FOUND))?;

    // nodes
    let nodes = get_nodes(&state, form.clone()).await?;
    debug!("fetched {} nodes", nodes.len());
    let node_filter_str = NodeFilter {
        cluster_id: form.cluster_id.clone(),
        node_addresses: form.node_addresses.clone(),
    }
    .to_string();
    let log_path = new_log_path(form.command_id)?;
    // save job
    let j = job::ActiveModel {
        command_id: Set(form.command_id),
        node_nums: Set(nodes.len() as i32),
        node_filter: Set(node_filter_str),
        log_path: Set(String::from(log_path.clone().to_str().unwrap())),
        status: Set(1_i16),
        created_at: Set(Local::now().with_timezone(&FixedOffset::east(8 * 3600))),
        ..Default::default()
    };
    let job_id = job::Entity::insert(j)
        .exec(&state.conn)
        .await
        .map_err(InternalServerError)?
        .last_insert_id;

    let config = state.config.clone();
    // start running command
    std::thread::spawn(move || {
        debug!("start running command...........");
        let mut jobs = to_commands_jobs(nodes, command, config);
        let mut b = Batch::new(100, 10);
        b.with_jobs(jobs);
        let result = b.run();

        match File::create(log_path.clone()) {
            Ok(f) => {
                let mut stream = BufWriter::new(f);
                for r in result {
                    match r {
                        Ok(r) => match writeln!(&mut stream, "{}", r) {
                            Ok(_) => {}
                            Err(e) => {
                                error!("writing result[{}] failed: {}", r, e)
                            }
                        },
                        Err(e) => match writeln!(&mut stream, "{}", e) {
                            Ok(_) => {}
                            Err(e1) => {
                                error!("writing error[{}] failed: {}", e, e1)
                            }
                        },
                    }
                }
            }
            Err(e) => {
                error!(
                    "open log file [{}] error: {}",
                    log_path.to_str().unwrap(),
                    e
                )
            }
        }
        debug!("end running command...........");
    });

    Ok(StatusCode::FOUND.with_header("location", format!("/command-job/{}", job_id)))
}

#[handler]
pub async fn command_job(state: Data<&AppState>, Path(id): Path<i32>) -> Result<impl IntoResponse> {
    let job: job::Model = job::Entity::find_by_id(id)
        .one(&state.conn)
        .await
        .map_err(InternalServerError)?
        .ok_or_else(|| Error::from_status(StatusCode::NOT_FOUND))?;

    let command: Model = Entity::find_by_id(job.command_id)
        .one(&state.conn)
        .await
        .map_err(InternalServerError)?
        .ok_or_else(|| Error::from_status(StatusCode::NOT_FOUND))?;

    let log: String = tokio::fs::read_to_string(&job.log_path)
        .await
        .map_or(String::from(""), |x| x);

    let mut ctx = tera::Context::new();
    ctx.insert("command", &command);
    ctx.insert("job", &job);
    ctx.insert("log", &log);

    let body = state
        .templates
        .render("command/command-job.html.tera", &ctx)
        .map_err(InternalServerError)?;
    Ok(Html(body))
}

#[derive(Serialize, Deserialize)]
struct NodeFilter {
    cluster_id: String,
    node_addresses: String,
}

impl ToString for NodeFilter {
    fn to_string(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}

impl FromStr for NodeFilter {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let o: NodeFilter = serde_json::from_str(s)?;
        Ok(o)
    }
}

async fn get_nodes(state: &AppState, form: RunCommandForm) -> Result<Vec<node::Model>> {
    let condition = match form.cluster_id.as_str() {
        "" => Condition::all(),
        _ => Condition::all().add(node::Column::ClusterId.contains(&form.cluster_id)),
    };
    let condition = match form.node_addresses.as_str() {
        "" => condition,
        _ => {
            let addrs: Vec<&str> = form.node_addresses.split(',').collect();
            condition.add(node::Column::Address.is_in(addrs))
        }
    };

    let nodes = node::Entity::find()
        // .select_only()
        // .column(node::Column::Host)
        // .column(node::Column::Port)
        .filter(condition)
        .all(&state.conn)
        .await
        .map_err(InternalServerError)?;

    Ok(nodes)
}

impl From<(node::Model, &Model, &AppConfig)> for CommandsJob {
    fn from(mm: (node::Model, &Model, &AppConfig)) -> Self {
        let (node, command, config) = mm;
        CommandsJob {
            name: node.address.clone(),
            uri: format!(
                "mongodb://{}:{}@{}",
                config.mongodb_admin_username, config.mongodb_admin_password, node.address
            ),
            commands: str_to_commands(&command.script).unwrap_or_default(),
        }
    }
}

fn to_commands_jobs(
    nodes: Vec<node::Model>,
    command: Model,
    config: AppConfig,
) -> Vec<CommandsJob> {
    let mut jobs = Vec::new();
    for node in nodes {
        jobs.push((node, &command, &config).into());
    }
    jobs
}

fn new_log_path(command_id: i32) -> Result<PathBuf> {
    let mut p = PathBuf::new();
    p.push(COMMAND_JOB_LOG_ROOT);
    p.push(command_id.to_string());
    debug!("create log dir [{}]", p.to_str().unwrap());
    fs::create_dir_all(p.clone()).map_err(InternalServerError)?;

    let now = Utc::now();
    p.push(format!("{}.log", now.timestamp_millis()));

    Ok(p)
}
