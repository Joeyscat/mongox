use poem::endpoint::StaticFilesEndpoint;
use poem::{get, listener::TcpListener, post, EndpointExt, Route, Server};
use sea_orm::DatabaseConnection;
use std::env;
use std::io;
use tera::Tera;
use tracing::debug;

use crate::api::command::*;
use crate::api::node::*;
use crate::api::run_command::*;

mod api;
mod job;
mod middleware;
mod utils;

pub mod errors {
    pub use anyhow::Result;
}

#[derive(Debug, Clone)]
pub struct AppState {
    pub templates: tera::Tera,
    pub conn: DatabaseConnection,
    pub config: AppConfig,
}

#[derive(Debug, Clone)]
pub struct AppConfig {
    pub mongodb_admin_username: String,
    pub mongodb_admin_password: String,
}

#[tokio::main]
async fn main() -> io::Result<()> {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "poem=debug");
    }
    tracing_subscriber::fmt::init();

    // get env vars
    dotenv::dotenv().ok();
    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
    let host = env::var("HOST").expect("HOST is not set in .env file");
    let port = env::var("PORT").expect("PORT is not set in .env file");
    let server_url = format!("{}:{}", host, port);
    let mongodb_admin_username =
        env::var("MONGODB_ADMIN_USERNAME").expect("MONGODB_ADMIN_USERNAME is not set in .env file");
    let mongodb_admin_password =
        env::var("MONGODB_ADMIN_PASSWORD").expect("MONGODB_ADMIN_PASSWORD is not set in .env file");

    let conn = sea_orm::Database::connect(&db_url).await.unwrap();
    let templates = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();
    let config = AppConfig {
        mongodb_admin_username,
        mongodb_admin_password,
    };
    let state = AppState {
        templates,
        conn,
        config,
    };

    debug!("Starting server at {}", server_url);

    let app = Route::new()
        .at("/", get(index))
        .at("/run-command/:id", get(run_command))
        .at("/command-job/:id", get(command_job))
        .at("/run-command", post(do_run_command))
        .at("/command", post(create_command).get(list_command))
        .at("/command/new", get(new_command))
        .at("/command/:id", get(edit_command).post(update_command))
        .at("/command/delete/:id", post(delete_command))
        .at("/node", get(list_node))
        .at("/node/import", post(import_node))
        .nest(
            "/static",
            StaticFilesEndpoint::new(concat!(env!("CARGO_MANIFEST_DIR"), "/static")),
        )
        .data(state)
        .around(middleware::log);
    let server = Server::new(TcpListener::bind(format!("{}:{}", host, port)));
    server.run(app).await
}
