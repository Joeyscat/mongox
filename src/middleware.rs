use std::time::Instant;
use tracing::{debug, error};

use poem::{Endpoint, IntoResponse, Request, Response, Result};

pub async fn log<E: Endpoint>(next: E, req: Request) -> Result<Response> {
    let q = match req.uri().path_and_query() {
        Some(pq) => pq.as_str(),
        None => "",
    };
    // 200 | 2.336s | 127.0.0.1 | GET /api/v1/auth?user_id=asd
    let s = format!("{} | {} {}", req.remote_addr(), req.method(), q);
    let start = Instant::now();
    let res = next.call(req).await;
    let cost = start.elapsed().as_millis();

    match res {
        Ok(resp) => {
            let resp = resp.into_response();
            debug!("{} | {}ms | {}", resp.status(), cost, s);
            Ok(resp)
        }
        Err(err) => {
            error!("{} | {}ms | {} => {}", err, cost, s, err);
            Err(err)
        }
    }
}
