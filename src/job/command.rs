use std::{fmt::Display, time::Duration};

use mongodb::bson::{doc, Document};

use crate::errors::Result;

pub struct CommandsJob {
    pub name: String,
    /// 连接mongodb实例的uri
    pub uri: String,
    /// 待执行的命令
    pub commands: Vec<Document>,
}

pub struct CommandsJobResult {
    pub name: String,
    /// 每个命令执行的返回结果放到这里
    pub results: Vec<CommandResult>,
}

impl Display for CommandsJobResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "[{}]", self.name)?;
        for result in &self.results {
            write!(f, "{}", result)?;
        }
        Ok(())
    }
}

pub struct CommandResult {
    pub ok: bool,
    pub result: String,
}

impl Display for CommandResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let state = match self.ok {
            true => "OK",
            false => "FAILED",
        };
        writeln!(f, "[{}] - [{}]", state, self.result)?;
        Ok(())
    }
}

/// 对mongodb实例建立链接，并执行一个或多个命令，返回每个命令执行的结果
pub async fn run_job(job: CommandsJob) -> Result<CommandsJobResult> {
    let mut client_options = mongodb::options::ClientOptions::parse(job.uri).await?;
    client_options.connect_timeout = Some(Duration::from_secs(3));
    // client_options.max_idle_time = Some(Duration::from_secs(5));
    client_options.server_selection_timeout = Some(Duration::from_secs(3));
    let client = mongodb::Client::with_options(client_options)?;
    let db = client.database("admin");
    db.run_command(doc! {"ping": 1}, None).await?;

    let mut command_results = vec![];
    for command in job.commands {
        let r = db.run_command(command, None).await;
        let ok: bool;
        let r_str;
        if r.is_err() {
            ok = false;
            r_str = format!("{}", r.expect_err("need an error here"));
        } else {
            let re = r.expect("need a result here");
            r_str = re.to_string();
            ok = true;
        }

        command_results.push(CommandResult { ok, result: r_str });
    }

    Ok(CommandsJobResult {
        name: job.name,
        results: command_results,
    })
}

#[cfg(test)]
mod tests {
    use super::{run_job, CommandsJob};
    use bson::doc;

    #[tokio::test]
    async fn test_run_job() {
        let mut commands = Vec::new();
        commands.push(doc! {
            "createRole": "myClusterwideAdmin",
            "privileges": [
                {
                    "resource": {
                        "cluster": true
                    },
                    "actions": [
                        "addShard"
                    ]
                }
            ],
            "roles": [
                {
                    "role": "read",
                    "db": "admin"
                }
            ]
        });
        commands.push(doc! {
            "createRole1": "myClusterwideAdmin",
            "privileges": [ ],
            "roles": [ ]
        });

        let job = CommandsJob {
            name: "haha".to_string(),
            uri: "mongodb://admin:a0QgTFipMt9TbV6bNYX8@192.168.50.5:27017".to_string(),
            commands,
        };

        let result = run_job(job).await;
        assert!(result.is_ok());
        let results = result.unwrap().results;
        for r in results {
            println!("ok: {}", r.ok);
            println!("result: {}", r.result);
        }
    }
}
