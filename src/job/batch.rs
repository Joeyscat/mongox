use crate::errors::Result;
use std::thread;
use tokio::runtime::Runtime;

use chan::{Receiver, Sender};

use super::command::{run_job, CommandsJob, CommandsJobResult};

const BATCH_MAX_THREAD_NUMS: usize = 100;

pub struct Batch {
    thread_nums: usize,
    jobs: Vec<CommandsJob>,

    job_sender: Sender<CommandsJob>,
    job_receiver: Receiver<CommandsJob>,
    result_sender: Sender<Result<CommandsJobResult>>,
    result_receiver: Receiver<Result<CommandsJobResult>>,
}

impl Batch {
    pub fn new(buf_size: usize, thread_nums: usize) -> Self {
        let thread_nums = match thread_nums > BATCH_MAX_THREAD_NUMS {
            true => BATCH_MAX_THREAD_NUMS,
            false => thread_nums,
        };

        let (job_sender, job_receiver) = chan::sync(buf_size);
        let (result_sender, result_receiver) = chan::sync(buf_size);

        Batch {
            thread_nums,
            jobs: Vec::new(),

            job_sender,
            job_receiver,
            result_sender,
            result_receiver,
        }
    }

    pub fn with_jobs(&mut self, mut jobs: Vec<CommandsJob>) {
        self.jobs.append(&mut jobs);
    }

    pub fn run(self) -> Receiver<Result<CommandsJobResult>> {
        let job_receiver = {
            let job_sender = self.job_sender.clone();
            thread::spawn(move || {
                for j in self.jobs {
                    job_sender.send(j);
                }
            });
            drop(self.job_sender);
            self.job_receiver
        };

        let wg = chan::WaitGroup::new();
        for _ in 0..self.thread_nums {
            wg.add(1);
            let wg = wg.clone();
            let job_receiver = job_receiver.clone();
            let result_sender = self.result_sender.clone();
            // let result_handler = self.result_handler.clone();
            thread::spawn(move || {
                for commands_job in job_receiver {
                    let result = Runtime::new().unwrap().block_on(run_job(commands_job));
                    // handle result
                    result_sender.send(result);
                }
                wg.done();
            });
        }

        wg.wait();
        drop(self.result_sender);
        self.result_receiver
    }
}

#[cfg(test)]
mod tests {

    use super::Batch;
    use crate::job::command::CommandsJob;
    use bson::doc;
    #[test]
    fn test_batch() {
        let mut b = Batch::new(1000, 10);
        let mut jobs = Vec::new();
        let mut commands = Vec::new();
        commands.push(doc! {
            "createRole": "myClusterwideAdmin",
            "privileges": [ ],
            "roles": [ ]
        });
        commands.push(doc! {
            "createRole1": "myClusterwideAdmin",
            "privileges": [ ],
            "roles": [ ]
        });
        jobs.push(CommandsJob {
            name: "haha".to_string(),
            uri: "mongodb://admin:a0QgTFipMt9TbV6bNYX8@192.168.50.5:27017".to_string(),
            commands: commands.clone(),
        });
        jobs.push(CommandsJob {
            name: "haha1".to_string(),
            uri: "mongodb://admin:a0QgTFipMt9TbV6bNYX8@192.168.50.5:27016".to_string(),
            commands: commands.clone(),
        });
        jobs.push(CommandsJob {
            name: "haha2".to_string(),
            uri: "mongodb://admin:a0QgTFipMt9TbV6bNYX8@192.168.50.5:27017".to_string(),
            commands: commands.clone(),
        });

        b.with_jobs(jobs);

        let result = b.run();
        for r in result {
            match r {
                Ok(r) => {
                    println!("--------job result: {}", r)
                }
                Err(e) => {
                    println!("--------job failed: {}", e)
                }
            }
        }
    }
}
