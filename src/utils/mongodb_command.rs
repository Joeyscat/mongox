#[cfg(test)]
mod tests {

    use crate::utils::ser::str_to_bson;

    #[tokio::test]
    async fn test_run_command() {
        let client = mongodb::Client::with_uri_str(
            "mongodb://admin:a0QgTFipMt9TbV6bNYX8@192.168.50.5:27017",
        )
        .await
        .unwrap();
        let db = client.database("admin");

        let s = std::fs::read_to_string("commands.json").unwrap();
        let v: serde_json::Value = serde_json::from_str(&s).unwrap();
        for ele in v.as_array().unwrap() {
            let ss = ele.to_string();
            let command = str_to_bson(&ss).unwrap();
            println!("{}\n", command);

            let r = db.run_command(command, None).await;

            if r.is_err() {
                let e = r.err().unwrap();
                println!("ERROR {}", e);
            }
        }
    }
}
