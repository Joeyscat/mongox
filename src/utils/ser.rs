use crate::errors::Result;
use anyhow::Ok;
use mongodb::bson::Document;

pub fn str_to_commands(s: &str) -> Result<Vec<Document>> {
    let val: serde_json::Value = serde_json::from_str(s)?;
    let mut r = Vec::new();
    let vv = val
        .as_array()
        .ok_or_else(|| anyhow::anyhow!("not an array: {}", s))?;
    for d in vv {
        r.push(str_to_bson(&d.to_string())?);
    }
    Ok(r)
}

pub fn str_to_bson(s: &str) -> Result<Document> {
    let m: serde_json::Map<String, serde_json::value::Value> = serde_json::from_str(s)?;
    let d = Document::try_from(m)?;
    Ok(d)
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_s() {
        let s = r###"
        {
            "createRole": "myClusterwideAdmin",
            "privileges": [
                {
                    "resource": {
                        "cluster": true
                    },
                    "actions": [
                        "addShard"
                    ]
                }
            ],
            "roles": [
                {
                    "role": "read",
                    "db": "admin"
                }
            ]
        }
        "###;
        let d = super::str_to_bson(s);
        assert!(d.is_ok());
        println!("{}", d.unwrap());
    }
}
