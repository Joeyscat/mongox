# 简介

MongoDB 命令执行

## command 列表
`GET /command` *(html)*

## 新增 command
`GET /command/new` *(html)*

## 执行 command
`GET /run-command/{command-id}` *(html)*

## command 执行结果
`GET /command-job/{job-id}` *(html)*

## node 列表
`GET /node` *(html)*

## 导入 node
`POST /node/import` *(json)*


# 如何启动
## 配置文件
编辑`.env`文件

## 运行项目
```shell
> RUST_LOG=info cargo run
```

## 访问首页
http://localhost:8001/