--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.4 (Ubuntu 14.4-1.pgdg21.10+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: command; Type: TABLE; Schema: public; Owner: mongox
--

CREATE TABLE public.command (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    script text NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone,
    deleted_at timestamp with time zone
);


ALTER TABLE public.command OWNER TO mongox;

--
-- Name: commands_id_seq; Type: SEQUENCE; Schema: public; Owner: mongox
--

ALTER TABLE public.command ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.commands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: job; Type: TABLE; Schema: public; Owner: mongox
--

CREATE TABLE public.job (
    id integer NOT NULL,
    command_id integer NOT NULL,
    node_filter character varying(255) NOT NULL,
    log_path character varying(255) NOT NULL,
    status smallint NOT NULL,
    created_at timestamp(6) with time zone NOT NULL,
    finished_at timestamp(6) with time zone,
    deleted_at timestamp(6) with time zone,
    node_nums integer NOT NULL
);


ALTER TABLE public.job OWNER TO mongox;

--
-- Name: COLUMN job.status; Type: COMMENT; Schema: public; Owner: mongox
--

COMMENT ON COLUMN public.job.status IS '1-running
2-success
3-failed';


--
-- Name: job_id_seq; Type: SEQUENCE; Schema: public; Owner: mongox
--

ALTER TABLE public.job ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: node; Type: TABLE; Schema: public; Owner: mongox
--

CREATE TABLE public.node (
    id integer NOT NULL,
    cluster_id character varying NOT NULL,
    host character varying(50) NOT NULL,
    port smallint NOT NULL,
    address character varying(100) NOT NULL,
    node_type character varying(10) NOT NULL,
    created_at timestamp(6) with time zone NOT NULL,
    updated_at timestamp with time zone,
    deleted_at timestamp(6) with time zone
);


ALTER TABLE public.node OWNER TO mongox;

--
-- Name: node_id_seq; Type: SEQUENCE; Schema: public; Owner: mongox
--

ALTER TABLE public.node ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: command commands_pkey; Type: CONSTRAINT; Schema: public; Owner: mongox
--

ALTER TABLE ONLY public.command
    ADD CONSTRAINT commands_pkey PRIMARY KEY (id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: public; Owner: mongox
--

ALTER TABLE ONLY public.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (id);


--
-- Name: node node_pkey; Type: CONSTRAINT; Schema: public; Owner: mongox
--

ALTER TABLE ONLY public.node
    ADD CONSTRAINT node_pkey PRIMARY KEY (id);


--
-- Name: clusterid_host_port_idx; Type: INDEX; Schema: public; Owner: mongox
--

CREATE UNIQUE INDEX clusterid_host_port_idx ON public.node USING btree (cluster_id, host, port);


--
-- Name: name_uniq_idx; Type: INDEX; Schema: public; Owner: mongox
--

CREATE INDEX name_uniq_idx ON public.command USING btree (name);


--
-- PostgreSQL database dump complete
--

